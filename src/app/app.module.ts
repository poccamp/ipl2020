import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PlayerStatisticsComponent } from './player-statistics/player-statistics.component';
import { TeamStatisticsComponent } from './team-statistics/team-statistics.component';
import { BiddingStatisticsComponent } from './bidding-statistics/bidding-statistics.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    PlayerStatisticsComponent,
    TeamStatisticsComponent,
    BiddingStatisticsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
